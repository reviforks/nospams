
NoSpam is a service for real-time detection of blog comment spam.

It works without relying on the reputation of the submitter's IP
address, which is a common limitation in the more popular spam
filtering services that doesn't allow for anonymous publication.

The filtering is based on a mix of Bayesian classification,
analysis of external URLs, and a simple rule engine. We feel that
its results are accurate enough, even without having the poster's
IP address available.

This software was heavily inspired by
`blogspam.net <http://blogspam.net/>`_ and it actually offers a
compatible `XML-RPC API <documentation>`_.


