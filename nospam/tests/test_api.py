from nospam.api import NoSpam
import os
import shutil
import tempfile
import unittest


class TestAPI(unittest.TestCase):

    def setUp(self):
        self.tmp_dir = tempfile.mkdtemp()
        self.config = {'plugin_dir': os.path.join(os.path.dirname(
                         __file__), 'test_plugins'),
                       'spam_threshold': 1,
                       'conf_dir': self.tmp_dir, 'data_dir': self.tmp_dir}
        self.ns = NoSpam(self.config)

    def tearDown(self):
        shutil.rmtree(self.tmp_dir)

    def test_plugin_load(self):
        self.assertEquals(1, len(self.ns._plugins))
        self.assertEquals('SamplePlugin', self.ns._plugins[0].__class__.__name__)

    def test_comment_spam(self):
        comment = {'comment': 'spam', 'site': 's'}
        is_spam, score, msg = self.ns.test(comment)
        self.assertTrue(is_spam)

    def test_comment_spam_below_threshold(self):
        self.ns._spam_threshold = 2.0
        comment = {'comment': 'spam', 'site': 's'}
        is_spam, score, msg = self.ns.test(comment)
        self.assertFalse(is_spam)

    def test_comment_nospam(self):
        comment = {'comment': 'something else', 'site': 's'}
        is_spam, score, msg = self.ns.test(comment)
        self.assertFalse(is_spam)

    def test_classify_noop(self):
        # just test that this doesn't raise any exception
        comment = {'comment': 'something', 'train': 'ok', 'site': 's'}
        self.ns.classify(comment)

    def test_find_urls(self):
        comment = {'comment': 'yeah http://inventati.org yeah', 'site': 's'}
        is_spam, score, msg = self.ns.test(comment)
        self.assert_('_urls' in comment)
        self.assertEquals('http://inventati.org', comment['_urls'][0])

        comment = {'comment': 'blah', 'site': 's',
                   'link': 'http://inventati.org'}
        is_spam, score, msg = self.ns.test(comment)
        self.assert_('_urls' in comment)
        self.assertEquals('http://inventati.org', comment['_urls'][0])

    def test_counters_survive_restart(self):
        self.ns._incr_counter('test')
        del self.ns
        self.ns = NoSpam(self.config)
        self.ns._incr_counter('test')
        self.assertEquals(
            2, self.ns._counters['nospam_comments{kind="test",site="@all"}'])

    def test_stats(self):
        self.ns._incr_counter('spam', 'example.org')
        self.ns._incr_counter('spam', 'example.org')
        self.ns._incr_counter('ham', 'example.org')
        ham, spam = self.ns.stats('example.org')
        self.assertEquals(1, ham)
        self.assertEquals(2, spam)


if __name__ == '__main__':
    unittest.main()
        
