import os
import shutil
import tempfile
import unittest
from nospam.plugins.classified import ClassifiedPlugin


class ClassifiedPluginTest(unittest.TestCase):

    def setUp(self):
        self.data_dir = tempfile.mkdtemp()
        config = {'data_dir': self.data_dir,
                  'classified_max_new_links': 10}
        self.cp = ClassifiedPlugin(config)

    def tearDown(self):
        shutil.rmtree(self.data_dir)

    def test_no_links(self):
        comment = {'_urls': []}
        score, msg = self.cp.testComment(comment)
        self.assertEquals(0, score)

    def test_too_many_links(self):
        comment = {'_urls': ['http://www.blah.com'], 'train': 'spam'}
        for i in range(11):
            self.cp.classifyComment(comment)
        score, msg = self.cp.testComment(comment)
        self.assert_(score > 0)
