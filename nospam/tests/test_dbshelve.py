import os
import shutil
import tempfile
import unittest

# Make the test conditional on successful bsddb3 import.
try:
    from bsddb3 import db
except ImportError:
    from nose.exc import SkipTest
    raise SkipTest('LevelDB not found')

from nospam.dbshelve import dbshelve
from nospam.dbshelve import connect_db


class TestDbshelve(unittest.TestCase):

    def setUp(self):
        self.shelve = dbshelve()
        self.dbdir = tempfile.mkdtemp()
        self.shelve.open(os.path.join(self.dbdir, 'test'))

    def tearDown(self):
        self.shelve.close()
        shutil.rmtree(self.dbdir)
        
    def test_open(self):        
        self.assertEquals(True, isinstance(self.shelve._env, connect_db))

    def test_read_write(self):
        self.shelve['pippo'] = 'pluto'
        self.assertEquals('pluto', self.shelve['pippo'])

    def test_delete(self):
        del self.shelve['pippo']
        dict = lambda d: d['pippo'] 
        self.assertRaises(KeyError, dict, self.shelve)

    def test_rw_struct(self):
        self.shelve['pippo'] = [2,4,6,8, 'pluto']
        self.assertEquals(6,self.shelve['pippo'][2])

    def test_keys(self):
        self.shelve['pippo'] = 1
        self.shelve['pluto'] = 1        
        self.assertEquals(['pippo', 'pluto'], self.shelve.keys())

    def test_add(self):
        self.shelve['pippo'] = 1
        self.shelve['pippo'] += 1
        self.assertEquals(2, self.shelve['pippo'])

if __name__ == '__main__':
    unittest.main()
