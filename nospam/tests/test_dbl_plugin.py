import os
import shutil
import tempfile
import unittest
from nospam.plugins.dbl import DBLPlugin


class DBLPluginTest(unittest.TestCase):

    def setUp(self):
        self.data_dir = tempfile.mkdtemp()
        config = {'data_dir': self.data_dir}
        self.cp = DBLPlugin(config)

    def tearDown(self):
        shutil.rmtree(self.data_dir)

    def test_no_links(self):
        score, msg = self.cp.testComment({'_urls': []})
        self.assertEquals(0, score)

    def test_link(self):
        score, msg = self.cp.testComment({'_urls': ['http://www.autistici.org/']})
        self.assertEquals(0, score)
        # URL normalization removes the www prefix.
        self.assertTrue('autistici.org' in self.cp._cache)
