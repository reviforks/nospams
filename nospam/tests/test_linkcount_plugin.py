import os
import tempfile
import unittest
from nospam import urls
from nospam.plugins.linkcount import LinkCountPlugin


class LinkCountPluginTest(unittest.TestCase):

    def setUp(self):
        config = {'max_links': 2}
        urls._domain_whitelist = set(['youtube.com'])
        self.lc = LinkCountPlugin(config)

    def test_no_links_ok(self):
        comment = {'_urls': []}
        score, msg = self.lc.testComment(comment)
        self.assertEquals(0, score)

    def test_whitelisted_links_ok(self):
        comment = {'_urls': ['http://www.youtube.com/?v=1234',
                             'http://www.youtube.com/?v=3456',
                             'http://www.youtube.com/?v=4567']}
        score, msg = self.lc.testComment(comment)
        self.assertEquals(0, score)

    def test_duplicated_links(self):
        comment = {'_urls': ['http://blah.blah'] * 10}
        score, msg = self.lc.testComment(comment)
        self.assertEquals(0, score)

    def test_too_many_links(self):
        comment = {'_urls': ['http://one',
                             'http://two',
                             'http://three']}
        score, msg = self.lc.testComment(comment)
        self.assert_(score > 0)
