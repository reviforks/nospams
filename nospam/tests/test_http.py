from nospam.flaskext.xmlrpc import XMLRPCTester
from nospam import server
import os
import shutil
import tempfile
import unittest


class RequestStub(object):

    def __init__(self, uri, method):
        self.uri = uri
        self.typestr = method
        self.out_hdrs = {}

    def add_output_header(self, hdr, value):
        self.out_hdrs[hdr] = value

    def send_reply(self, code, msg, data):
        self.reply_code = code
        self.reply_msg = msg
        self.reply = data


class HttpTest(unittest.TestCase):

    def setUp(self):
        self.tmp_dir = tempfile.mkdtemp()
        config = {'plugin_dir': os.path.join(os.path.dirname(
                    __file__), 'test_plugins'),
                  'conf_dir': self.tmp_dir, 'data_dir': self.tmp_dir,
                  'port': 44324}
        self.server = server.make_server(config)
        self.app = server.app.test_client()

    def tearDown(self):
        shutil.rmtree(self.tmp_dir)

    def test_index(self):
        resp = self.app.get('/')
        self.assertEquals(200, resp.status_code)
        self.assert_(b'NoSpam' in resp.data)

    def test_404(self):
        resp = self.app.get('/does-not-exist')
        self.assertEquals(404, resp.status_code)

    def test_doc(self):
        resp = self.app.get('/docs/about')
        self.assertEquals(200, resp.status_code)
        self.assert_(b'About' in resp.data)

    def test_vars(self):
        resp = self.app.get('/metrics')
        self.assertEquals(200, resp.status_code)

    def test_rpc2(self):
        resp = self.app.post('/RPC2')
        self.assertEquals(200, resp.status_code)

    def test_xmlrpc_testComment(self):
        xt = XMLRPCTester(self.app, '/RPC2')
        result = xt('testComment', {'site': 'cavallette.noblogs.org',
                                    'comment': 'test comment'})
        self.assertEquals('OK', result)
