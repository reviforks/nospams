import codecs


def read_file(path, encoding='utf-8'):
    """Read all lines from a file, except empty lines and comments.

    Returns list of lines (unicode strings), newlines are stripped.
    """
    fd = codecs.open(path, 'r', encoding)
    output = []
    try:
        for line in fd:
            line = line.strip()
            if not line or line.startswith('#'):
                continue
            output.append(line)
        return output
    finally:
        fd.close()

