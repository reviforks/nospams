#!/bin/sh

dir=${NOSPAM_SURBL_DIR:-/var/lib/nospam}

test -d ${dir} || mkdir -p ${dir}

if [ ! -e ${dir}/two-level-tlds ]; then
    /usr/bin/update-surbl
fi

exec /usr/local/bin/nospamd "$@"
