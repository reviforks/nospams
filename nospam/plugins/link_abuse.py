from nospam.plugin_base import BasePlugin


class LinkAbusePlugin(BasePlugin):

    def testComment(self, comment):
        # Does the comment have a link, which appears again in the
        # text of the comment?
        if ('link' in comment and comment['link'] and
            comment['link'] in comment['comment']):
            return (1, 'link_abuse')
        return (0, 'OK')
