import gevent
import os
import socket
try:
    import urlparse
except ImportError:
    from urllib import parse as urlparse

from nospam.dbshelve import get_shelve
from nospam.plugin_base import BasePlugin
from nospam import urls


class SURBLPlugin(BasePlugin):
    """SURBL plugin.

    Tries to follow as strictly as possible the implementation
    guidelines at http://www.surbl.org/implementation.html.
    """

    def __init__(self, config):
        data_dir = config.get('data_dir', './data')
        self._surbl_domain = config.get('surbl_domain', 'multi.surbl.org')
        self._cache = get_shelve(os.path.join(data_dir, 'surbl_cache'),
                                 config.get('concurrent', False))

    def _normalize_ips(self, host):
        if urls.is_ipaddr(host):
            parts = host.split('.')
            parts.reverse()
            return '.'.join(parts)
        return host

    def testComment(self, comment):
        if '_urls' not in comment:
            return (0, 'OK')

        results = {}
        def surbl_lookup(domain):
            if domain not in self._cache:
                try:
                    q = '%s.%s' % (domain, self._surbl_domain)
                    res = socket.gethostbyname(q)
                    self._cache[domain] = True
                except socket.gaierror:
                    self._cache[domain] = False
            results[domain] = self._cache[domain]

        # Perform all the asynchronous DNS lookups in parallel.
        domains = set(str(self._normalize_ips(d))
                      for d in urls.get_domains(comment['_urls']))
        lookups = [gevent.spawn(surbl_lookup, domain) for domain in domains]
        gevent.joinall(lookups)

        score = 0
        badurls = []
        for url, is_bad in results.items():
            if is_bad:
                score += 1
                badurls.append(url)
        if score:
            return (score, 'SURBL:%s' % ','.join(badurls))
        else:
            return (0, 'OK')
