import gevent
import os
import re
import socket
from nospam.dbshelve import get_shelve
try:
    import urlparse
except ImportError:
    from urllib import parse as urlparse

from nospam.plugin_base import BasePlugin
from nospam import urls


class DBLPlugin(BasePlugin):
    """Lookup URLs in the Spamhaus DBL list."""

    # Keep disabled for now, doesn't seem to be very effective.
    enabled = False

    def __init__(self, config):
        data_dir = config.get('data_dir', './data')
        concurrent = config.get('concurrent', False)
        self._dbl_domain = config.get('dbl_domain', 'dbl.spamhaus.org')
        self._cache = get_shelve(os.path.join(data_dir, 'dbl_cache'), concurrent)

    def _normalize_domain(self, host):
        # Do not lookup IP addresses on DBL.
        if urls.is_ipaddr(host):
            return None
        return host

    def testComment(self, comment):
        if '_urls' not in comment:
            return (0, 'OK')

        results = {}
        def surbl_lookup(domain):
            if not domain:
                results[domain] = False
            if domain not in self._cache:
                try:
                    q = '%s.%s' % (domain, self._dbl_domain)
                    res = socket.gethostbyname(q)
                    self._cache[domain] = True
                except socket.gaierror:
                    self._cache[domain] = False
            results[domain] = self._cache[domain]

        # Perform all the asynchronous DNS lookups in parallel.
        domains = set(str(self._normalize_domain(d))
                      for d in urls.get_domains(comment['_urls']))
        lookups = [gevent.spawn(surbl_lookup, domain) for domain in domains]
        gevent.joinall(lookups)

        score = 0
        badurls = []
        for url, is_bad in results.items():
            if is_bad:
                score += 1
                badurls.append(url)
        if score:
            return (score, 'DBL:%s' % ','.join(badurls))
        else:
            return (0, 'OK')
