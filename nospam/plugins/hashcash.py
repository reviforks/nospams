import re
from nospam.plugin_base import BasePlugin


class HashCashPlugin(BasePlugin):
    '''This plugin tests for the results of the hashcash spam-preventing
    plugin of wordpress, allowing to integrate it in our scoring'''

    def __init__(self, config):
        self._hashcashre = re.compile(
            r'/\[WORDPRESS HASHCASH\] The poster sent us .* which is not a hashcash value/',
            re.U | re.M | re.I)

    def testComment(self, comment):
        if self._hashcashre.match(comment['comment']):
            return (self.SCORE, "Hashcash warning")
        return (0, "OK")
