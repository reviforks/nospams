from nospam.plugin_base import BasePlugin


class StrongPlugin(BasePlugin):

    def testComment(self, comment):
        txt = comment['comment']
        if txt.startswith('<strong>') or txt.startswith('<STRONG>'):
            return (self.SCORE, '<strong>')
        else:
            return (0, 'OK')
