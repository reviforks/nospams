import math
from nospam.plugin_base import BasePlugin
from nospam import utils
from nospam import urls


class LinkCountPlugin(BasePlugin):

    def __init__(self, config):
        self._max_links = int(config.get('max_links', '2'))

    def testComment(self, comment):
        # Count the number of unique links.
        link_count = len(set(urls.get_domains(comment['_urls'])))
        if link_count > self._max_links:
            score = self.SCORE * math.log(1.0 + link_count / float(self._max_links))
            return (score, "%d links found" % link_count)
        return (0, "OK")
