import os
from nospam.aux.crm import Classifier
from nospam.plugin_base import BasePlugin


class BayesianPlugin(BasePlugin):

    THRESHOLD = 0.9

    def __init__(self, config):
        crm_dir = os.path.join(config.get('data_dir', './data'), 'bayes')
        self._crm = Classifier(crm_dir, ['ok', 'spam'])

    def _comment_text(self, comment):
        # Accumulate tokens from the comment in the text fed to the
        # Bayes classificator.
        out = [comment['comment']]
        for attr in ('name', 'link', 'email', 'agent'):
            if comment.get(attr):
                out.append(comment[attr])
        return '\n'.join(out)

    def testComment(self, comment):
        text = self._comment_text(comment)
        (category, probability) = self._crm.classify(text)
        if probability >= self.THRESHOLD:
            if category == 'spam':
                return (self.SCORE, "bayes")
        return (0, "OK")

    def classifyComment(self, comment):
        self._crm.learn(comment['train'],
                        self._comment_text(comment))
