import os
import re
try:
    import urlparse
except ImportError:
    import urllib.parse as urlparse
from nospam import utils


_urls = '(?: %s)' % '|'.join("""http https telnet gopher file wais
ftp""".split())
_ltrs = r'\w'
_gunk = r'/#~:.?+=&%@!\-'
_punc = r'.:?!\-'
_any = "%(ltrs)s%(gunk)s%(punc)s" % { 'ltrs' : _ltrs,
                                      'gunk' : _gunk,
                                      'punc' : _punc }

_url = r"""
    \b                            # start at word boundary
        %(urls)s    :             # need resource and a colon
        [%(any)s]  +?             # followed by one or more
                                  #  of any valid character, but
                                  #  be conservative and take only
                                  #  what you need to....
    (?=                           # look-ahead non-consumptive assertion
            [%(punc)s]*           # either 0 or more punctuation
            (?:   [^%(any)s]      #  followed by a non-url char
                |                 #   or end of the string
                  $
            )
    )
    """ % {'urls' : _urls,
           'any' : _any,
           'punc' : _punc }

_url_re = re.compile(_url, re.VERBOSE | re.MULTILINE)
_ipaddr_re = re.compile(r'^(?:[0-9]{1,3})(?:\.[0-9]{1,3}){3}$')
_two_level_tlds = _three_level_tlds = _domain_whitelist = set()


def init_urls(config):
    """Initialize the URL-detection subsystem from config."""
    global _two_level_tlds, _three_level_tlds, _domain_whitelist
    data_dir = config.get('data_dir', './data')
    config_dir = config.get('config_dir', './conf')
    _two_level_tlds = set(utils.read_file(os.path.join(
                data_dir, 'two-level-tlds'), encoding='iso-8859-1'))
    _three_level_tlds = set(utils.read_file(os.path.join(
                data_dir, 'three-level-tlds'), encoding='iso-8859-1'))
    try:
        _domain_whitelist = set(utils.read_file(os.path.join(
                    config_dir, 'domain-whitelist')))
    except:
        pass


def find_urls(s):
    """Find all URLs in some text."""
    return _url_re.findall(s)


def is_ipaddr(domain):
    """Returns True if the input looks like an IP address."""
    return _ipaddr_re.search(domain)


def strip_subdomain(domain):
    """Reduce a domain to its shortest significant top-level form."""
    parts = domain.split('.')
    if len(parts) > 3:
        tmp = '.'.join(parts[-3:])
        if tmp in _three_level_tlds:
            return '.'.join(parts[-4:])
    if len(parts) > 2:
        tmp = '.'.join(parts[-2:])
        if tmp in _two_level_tlds:
            return '.'.join(parts[-3:])
    return '.'.join(parts[-2:])


def get_domains(url_list):
    for url in url_list:
        domain = urlparse.urlparse(url).netloc.split(':', 1)[0].lower()
        # Skip empty domains (internal links).
        if not domain:
            continue
        # Only apply domain-name normalization if this does not look
        # like a numerical IP address.
        if not is_ipaddr(domain):
            # Strip subdomain.
            domain = strip_subdomain(domain)
            # Skip whitelisted domains.
            if domain in _domain_whitelist:
                continue
        yield domain

