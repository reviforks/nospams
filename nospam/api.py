import glob
import logging
import os
import traceback
try:
    import urlparse
except ImportError:
    from urllib import parse as urlparse

from nospam import dbshelve
from nospam import plugin_base
from nospam import urls
from nospam import instrumentation

log = logging.getLogger(__name__)


def _extract_domain(url):
    if url.startswith('http'):
        return urlparse.urlsplit(url).netloc
    return url


class NoSpam(object):

    def __init__(self, config):
        self._config = config
        self._spam_threshold = float(config.get('spam_threshold', 2))
        self._autolearn_threshold = float(config.get('autolearn_threshold', 3))
        plugin_dir = config.get('plugin_dir',
                                os.path.join(os.path.dirname(__file__), 'plugins'))
        self._load_plugins(plugin_dir)
        # Provide some counters (and plug them into instrumentation)
        data_dir = config.get('data_dir', './data')
        concurrent = config.get('concurrent', False)
        self._counters = dbshelve.get_shelve(
            os.path.join(data_dir, 'counters'), concurrent)

    def _load_plugins(self, plugin_dir):
        self._plugins = []
        for filename in glob.glob(plugin_dir + '/*.py'):
            ctx = {}
            try:
                # Used to be just an execfile().
                with open(filename, 'r') as f:
                    code = compile(f.read(), filename, 'exec')
                    exec(code, ctx, ctx)
            except Exception as e:
                log.error('could not load plugin %s: %s', filename, e)
                continue
            # Find subclasses of BasePlugin.
            for key, value in ctx.items():
                if (isinstance(value, type)
                    and issubclass(value, plugin_base.BasePlugin)
                    and value != plugin_base.BasePlugin):
                    log.debug('found %s plugin', key)
                    if not value.enabled:
                        continue
                    try:
                        log.debug('enabling %s plugin', key)
                        self._plugins.append(value(self._config))
                    except Exception as e:
                        log.error('could not initialize plugin %s: %s', key, e)

    def _extract_urls(self, comment):
        all_urls = []
        if comment.get('link'):
            all_urls.append(comment['link'])
        all_urls += urls.find_urls(comment['comment'])
        return all_urls

    def _incr_counter(self, kind, site=None):
        all_key = 'nospam_comments{kind="%s",site="@all"}' % kind
        self._counters[all_key] = self._counters.get(all_key, 0) + 1
        instrumentation.incr_counter(all_key)
        if site:
            site_key = 'nospam_comments{kind="%s",site="%s"}' % (kind, site)
            instrumentation.incr_counter(site_key)
            self._counters[site_key] = self._counters.get(site_key, 0) + 1

    def test(self, comment):
        """Test the comment for spamminess.

        The 'comment' dictionary must contain at least a 'comment'
        key, containint the full text of the comment. Other known
        attributes are: 'name', 'title', 'link', 'email', 'agent'.

        Returns a (is_spam, spam_score, spam_message) tuple containing
        the result of the classification.
        """
        # Find all the outgoing links in the comment text, and add
        # them to the comment dictionary under the '_urls' key.
        comment['_urls'] = self._extract_urls(comment)
        # Compute the 'nice' site name (domain only, no http:// prefix).
        site = _extract_domain(comment['site'])
        # Test all plugins, then compare the total score against the
        # spam threshold.
        results = []
        for plugin in self._plugins:
            result = plugin.testComment(comment)
            # Keep track of the match counts for each plugin.
            if result[0] > 0:
                instrumentation.incr_counter(
                    'nospam_plugin_match{plugin="%s"}' % (
                        plugin.__class__.__name__,))
            results.append(result)
        tot_score = sum(map(lambda x: x[0], results))
        if tot_score >= self._spam_threshold:
            if tot_score >= self._autolearn_threshold:
                comment['train'] = 'spam'
                self.classify(comment)
            msg = ', '.join([x[1] for x in results if x[0] > 0])
            log.info('spam: site=%s score=%g %s', site, tot_score, msg)
            self._incr_counter('spam', site)
            return (True, tot_score, msg)
        self._incr_counter('ham', site)
        return (False, 0, None)

    def classify(self, comment):
        instrumentation.incr_counter('nospam_classify{train="%s"}' % (
            comment.get('train', 'err'),))
        comment['_urls'] = self._extract_urls(comment)
        for plugin in self._plugins:
            plugin.classifyComment(comment)

    def stats(self, site=None):
        if not site:
            site = '@all'
        return (
            self._counters.get('nospam_comments{kind="ham",site="%s"}' % site, 0),
            self._counters.get('nospam_comments{kind="spam",site="%s"}' % site, 0))


class BlogSpamCompatibleAPI(object):

    def __init__(self, ns):
        self._ns = ns

    def testComment(self, comment):
        instrumentation.incr_counter('rpc_count{type="testComment"}')
        try:
            for attr in ('comment', 'site'):
                if attr not in comment:
                    raise Exception('missing required attribute %s' % attr)
            is_spam, score, msg = self._ns.test(comment)
            if is_spam:
                return 'SPAM:%s' % msg
            return 'OK'
        except Exception as e:
            log.error('testComment: %s\n%s', e, traceback.format_exc())
            return 'ERROR:%s' % str(e)

    def classifyComment(self, comment):
        instrumentation.incr_counter('rpc_count{type="classifyComment"}')
        try:
            for attr in ('comment', 'site', 'train'):
                if attr not in comment:
                    raise Exception('missing required attribute %s' % attr)
            self._ns.classify(comment)
            return 'OK'
        except Exception as e:
            log.error('classifyComment: %s\n%s', e, traceback.format_exc())
            return 'ERROR:%s' % str(e)

    def getStats(self):
        ham, spam = self._ns.stats()
        return {'total': (ham + spam), 'ok': ham, 'spam': spam}


class JSONAPI(object):

    def __init__(self, ns):
        self._ns = ns

    def test_comment(self, comment):
        instrumentation.incr_counter('rpc_count{type="testComment"}')
        for attr in ('comment', 'site'):
            if attr not in comment:
                raise Exception('missing required attribute %s' % attr)
        is_spam, score, msg = self._ns.test(comment)
        result = {'status': 'spam' if is_spam else 'ok', 'score': score}
        if is_spam:
            result['message'] = msg
        return result

    def classify_comment(self, comment):
        instrumentation.incr_counter('rpc_count{type="classifyComment"}')
        for attr in ('comment', 'site', 'train'):
            if attr not in comment:
                raise Exception('missing required attribute %s' % attr)
        self._ns.classify(comment)
        return {}
