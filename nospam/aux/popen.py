#!/usr/bin/env python
"""Non-blocking subprocess.Popen() implementation.

Based on processes.py by Marcus Cavanaugh.
See http://groups.google.com/group/gevent/browse_thread/thread/7fca7230db0509f6
where it was first posted.
"""

import gevent
from gevent import socket

import subprocess
import errno
import sys
import os
import fcntl

import time
import logging
log = logging.getLogger(__name__)

BLOCKSIZE = 4096

try:
    unicode
except NameError:
    unicode = str


def popen_nb(args, data=''):
    """Communicate with the process."""
    # Convert unicode data to utf-8
    if isinstance(data, unicode):
        data = data.encode('utf-8')

    p = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, 
                         stderr=subprocess.STDOUT, shell=True, close_fds=True)
    log.debug('%s spawn PID %d', time.strftime('%H:%M:%S'), p.pid)
    fcntl.fcntl(p.stdin, fcntl.F_SETFL, os.O_NONBLOCK)  # make the file nonblocking
    fcntl.fcntl(p.stdout, fcntl.F_SETFL, os.O_NONBLOCK)  # make the file nonblocking

    bytes_total = len(data)
    bytes_written = 0
    while bytes_written < bytes_total:
        try:
            # p.stdin.write() doesn't return anything, so use os.write.
            bytes_written += os.write(p.stdin.fileno(), data[bytes_written:])
            log.debug('%s written %d (out of %d) to PID %d', time.strftime('%H:%M:%S'), bytes_written, bytes_total, p.pid)
        except IOError as ex:
            if ex[0] != errno.EAGAIN:
                raise
            sys.exc_clear()
        socket.wait_write(p.stdin.fileno())

    p.stdin.close()
    log.debug('%s closed stdin for PID %d', time.strftime('%H:%M:%S'), p.pid)
    log.debug('%s sent the following data to PID %d: %s', time.strftime('%H:%M:%S'), p.pid, data)

    chunks = []

    while True:
        try:
            chunk = p.stdout.read(BLOCKSIZE)
            if not chunk:
                break
            log.debug('%s read %d from PID %d', time.strftime('%H:%M:%S'), len(chunk), p.pid)
            chunks.append(chunk)
        except IOError as ex:
            if ex[0] != errno.EAGAIN:
                raise
            sys.exc_clear()
        socket.wait_read(p.stdout.fileno())

    p.stdout.close()
    log.debug('%s closed stdout for PID %d', time.strftime('%H:%M:%S'), p.pid)
    return b''.join(chunks)

